%define debug_package %{nil}

Name:           kubie
Version:        0.24.0
Release:        1%{?dist}
Summary:        A more powerful alternative to kubectx and kubens
Group:          Applications/System
License:        zlib
URL:            https://blog.sbstp.ca/introducing-kubie/
Source0:        https://github.com/sbstp/%{name}/archive/refs/tags/v%{version}.tar.gz
Source1:        https://github.com/sbstp/%{name}/releases/download/v%{version}/%{name}-linux-amd64

%description
A more powerful alternative to kubectx and kubens

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.23.0

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.22.0

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.20.1

* Fri Feb 03 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.19.3

* Tue Nov 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.19.1

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
